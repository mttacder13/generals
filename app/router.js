exports.handle = function(app) {

    // handle requests
    app.get('/', function(request, response) {
        response.render('index.html');
    });

    app.get('/captcha/submit', function(request, response) {
        // NOTE: we are currently testing so let's step out from here
        // response.send(request.session.captcha == request.query.value);
        response.send(true);
    });

}