// import the express module
var express = require('express');

// Import HTTP module
var http = require('http');

// Create a new Express application
var app = express();

// Create a Node.js based HTTP server on some specific port
// we set the port other than 80 so that we can easily
// switch coding from different platforms/osses/environments
var server = app.listen(3000);

// Create a Socket.IO server and attach it to the HTTP server
var io = require('socket.io').listen(server);

// Import our router module (which basically just serves our client files and the captcha)
var router = require('./router');

// Import the captcha module
var captcha = require('captcha');

// And finally, let's import our game IO handler (note, NOT the game module)
var gameIO = require('./game.io');

// configure our Express app
app.configure(function() {
    // Server all files under public folder to be static
    app.use(express.static(__dirname + '/public'));
    // Use Express' logger
    app.use(express.logger('dev'));
    // Express Cookie modules are required for our captcha (using session)
    app.use(express.cookieParser());
    app.use(express.cookieSession({ secret: 'redcat-gg' }));
    // Let's configure our captcha to below settings
    app.use(captcha({ url: '/captcha', color: '#8CC84B', background: '#009431' }));
});

// We will also use the packaged Express error handler
app.configure('development', function() {
    app.use(express.errorHandler());
});

// now let's handle requests
router.handle(app);

// We don't want to listen "too much" from Socket.IO logs
io.set('log level', 1);

// let's listen for connections. Once we receive one, start the game logic
io.sockets.on('connection', function(socket) {
    // Handle Socket.IO events and emit some
    gameIO.handle(io, socket);
});

// for unit testing, we will expose the server
exports.server = server;
