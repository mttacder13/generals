(function(i18n) {

// a hashset for internationalization
// this can be overriden somewhere
// before this script is included
var i18n = window.i18n || {};

// the main content div of HTML which can
// change from a welcome page to the game page
var content = $('.container .content');

// the client socket, take note of the port number
var socket = io.connect('http://localhost:3000');

// the game data containing the following:
//      id:         the current game ID
//      playerId:   the ID of the player for this client
//      playerName: the name of the player for this client
//      board:      the GameBoard view object, see board.js
var game = {};

/**
 * Translates the text to a targetted language.
 * Defaults to the language being used in creating the application (en)
 * @param  {String} text The text to translate
 * @return {String}      The translated text
 */
function _(text) {
    // let's see if there is a transalation
    text = i18n[text] ? i18n[text] : text;

    // now let's replace placeholders (%s) with the arguments passed
    var argIndex = 1;
    while (text.indexOf('%s') != -1 && argIndex < arguments.length) {
        text = text.replace('%s', arguments[argIndex]);
        argIndex++;
    }

    // return the possibly translated text
    return text;
}

/**
 * Changes the main content of the HTML with animation (fading)
 * @param  {String}   templateId the template ID
 * @param  {Function} callback   the method to call once the
 *                               transition is done
 */
function changeView(templateId, callback) {
    content.fadeOut('fast', function() {
        content.html($('#' + templateId).html());
        content.fadeIn('fast', function() {
            if (callback) {
                callback();
            }
        });
    });
}

/**
 * Set's the game's main message (top of the board)
 * @param {String} message The message text (or HTML) to display
 */
function setGameMessage(message) {
    // we will cache this jQuery object for a very minute performance improvement
    setGameMessage.element = setGameMessage.element || $('.game-message');
    // add a little fading animation when transitioning from and to different messages
    setGameMessage.element.fadeOut('fast', function() {
        setGameMessage.element.html(message).fadeIn();
    });
}

/***************************************************
    ALL SOCKET IO EVENT HANDLERS
***************************************************/

/**
 * handles "created" IO event, this method is only called when the client creates the game
 * data: gameId, playerId, playerName
 */
function onGameCreated(data) {
    if (data.success) {
        // let's update the view to the game UI
        changeView('game-template', function() {
            // let's notify the user that a game was successfully created
            setGameMessage(_('Welcome %s! Your game ID is %s. You must copy this game ID and send it to your opponent so he/she can join.', data.playerName, data.gameId));

            // then update some UI elements data
            $('.game-id').text(data.gameId);
            $('.your-name').text(data.playerName);

            // augment our game data with with the following data
            game.id = data.gameId;
            game.playerId = data.playerId;
            game.playerName = data.playerName;
            // and initialize our game board
            game.board = new GameBoard('#game-board', true);
        });
    } else {
        $.msgbox.show(
            _('Error encountered. The following error was encountered while creating the game: %s', data.message),
            _('No Game Created')
            );
    }
}

/**
 * handles the "player-joined" IO event
 * data: gameId, playerId, playerName
 */
function onPlayerJoined(data) {
    if (data.success) {
        // if we already have a game ID, or a player ID,
        // this means we are the game creator
        if (game.id) {
            // notify the current user
            setGameMessage(_('%s has joined your created game. Now, ARRANGE your game pieces and click on the READY button once done.', data.playerName));
            // enable the READY button
            $('#ready').removeAttr('disabled');
            // set the opponent name UI
            $('.opponent-name').text(data.playerName);
            // now, generate the pieces after a player joined,
            // we are set when we have 2 players so when another player joined,
            // we are good to generate the game pieces
            game.board.generatePieces();
        } else {
            // we are the challenger so update to the game view
            changeView('game-template', function() {
                // notify the current user
                setGameMessage(_('Welcome %s! You have successfully joined this game against %s. Now, ARRANGE your game pieces and click on the READY button once done.', data.playerName, data.opponentName));
                // enable the READY button
                $('#ready').removeAttr('disabled');
                // update UI elements to the current data
                $('.game-id').text(data.gameId);
                $('.your-name').text(data.playerName);
                $('.opponent-name').text(data.opponentName);
                // augment the game data with the current information
                game.id = data.gameId;
                game.playerId = data.playerId;
                game.playerName = data.playerName;
                // initialize our game board
                game.board = new GameBoard('#game-board', false);
                // now, generate the pieces after a player joined,
                // we are set when we have 2 players so when another player joined,
                // we are good to generate the game pieces
                game.board.generatePieces();
            });
        }
    } else {
        $.msgbox.show(
            _('Error encountered. The following error was encountered when a player attempts to join game: %s', data.message),
            _('Unable to Join Game'));
    }
}

function onPiecesSubmitted(data) {
    if (!data.success) {
        $.msgbox.show(_(data.message), _('Error encountered'));
        return;
    }
    if (data.playerId == game.playerId) {
        setGameMessage(_('Your pieces have been successfully submitted and validated.'));
        $('#ready').css('display', 'none');
    } else {
        setGameMessage(_('%s has submitted his/her game pieces.', $('.opponent-name').html()));

        // let's add them pieces in the board
        var elements = [];
        for (var i = 0; i < data.positions.length; i++) {
            elements.push(game.board.createGamePieceElement(data.positions[i]));
        }
        game.board.addGamePieceElements(elements);
    }
}

function onPlayerTakeTurn(data) {
    if (data.playerId == game.playerId) {
        setGameMessage(_('Your turn. Make your move.'));
        game.board.isLocked = false;
    } else {
        setGameMessage(_('Waiting for %s to make his/her move.', $('.opponent-name').html()));
        game.board.isLocked = true;
    }
}

function onPlayerLeft() {
    $.msgbox({
        message: _('%s left the game. This game session has been cancelled.', $('.opponent-name').html()),
        title: _('Opponent Left The Game'),
        onClosed: function() { window.location.reload(); }
    });
}

/***************************************************
    ALL HTML UI EVENT HANDLERS
***************************************************/

/**
 * handles the create-game button click
 */
function onGameCreate(e) {
    // let's prevent this event to bubble up
    e.preventDefault();

    // get the player name element wrapped in jQuery
    var playerName = $('#player-name-input');
    // get the captcha element wrapped in jQuery
    var captcha = $('.captcha input');

    // both fields are required so we must validate them first
    if (!playerName.val()) {
        $.msgbox.show(
            _('You must type in your player name so that your opponent can identify you.'),
            _('Player Name Required'),
            function() { playerName.focus(); });
        return;
    }
    if (!captcha.val()) {
        $.msgbox.show(
            _('The game server can only provide a finite set of simultaneous game sessions so as much as possible, we would like to prevent BOTS.'),
            _('CAPTCHA Required'),
            function() { captcha.focus(); });
        return;
    }

    // then we will send the captcha to the server through ajax and check if this client is not a BOT (okay, not foolproof)
    $.getJSON('/captcha/submit', { value: captcha.val() }, function(success) {
        if (success) {
            // captcha is correct so we emit the game-create IO event and passing our player name
            socket.emit('game-create', { playerName: playerName.val() });
        } else {
            // else, we refresh the captcha image
            $.msgbox.show(_('Invalid CAPTCHA value. Please try again.'), function() {
                $('.captcha img')[0].src = '/captcha';
                captcha.focus();
            });
        }
    });
}

/**
 * Handles the join-game button click event
 */
function onJoinGame(e) {
    // prevent event bubbling up
    e.preventDefault();

    // we need the game ID jQuery object
    var gameId = $('#game-id-input');
    // we also need the player name jQuery object
    var playerName = $('#player-name-input');

    // we will validate if both fields have values
    if (!playerName.val()) {
        $.msgbox.show(
            _('You must type in your player name so that your opponent can identify you.'),
            _('Player Name Required'),
            function() { playerName.focus(); });
        return;
    }
    if (!gameId.val()) {
        $.msgbox.show(_('The GAME ID is required so you can join that particular GAME SESSION.'), function() { gameId.focus(); });
        return;
    }

    // okay, we will emit the player-join IO event passing the game ID and our player name
    socket.emit('player-join', { gameId: gameId.val(), playerName: playerName.val() });
}

/**
 * Handles the READY button click event
 */
function onSubmitPieces(e) {
    // prevent event bubbling up
    e.preventDefault();

    var pieces = game.board.getAllGamePieces();

    socket.emit('submit-pieces', {
        gameId: game.id,
        playerId: game.playerId,
        gamePieces: pieces
    });
}

// handle socket IO events
socket.on('game-created', onGameCreated);
socket.on('player-joined', onPlayerJoined);
socket.on('pieces-submitted', onPiecesSubmitted);
socket.on('player-take-turn', onPlayerTakeTurn);
socket.on('player-left', onPlayerLeft);

// handle UI events
content.delegate('#create-game', 'click', onGameCreate);
content.delegate('#join-game', 'click', onJoinGame);
content.delegate('#ready', 'click', onSubmitPieces);

// now let's show our initial UI, the welcome content
changeView('template-welcome');

})(window, document, jQuery);
