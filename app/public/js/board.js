/**
 * GameBoard
 *     by Marjun T. Tacder
 *     mttacder@gmail.com
 *
 * This file contains all the source codes for all game components
 * like the GamePiece and the GameBoard. They are basically objects
 * wrapping the UI components as well as behavior.
 *
 * Requires: jQuery
 *
 * Please take note that we are not directly referring to an HTMLElementObject
 * in most cases. We are trying to utilize jQuery most of the time that is
 * why we don't use $ prefix (or suffix) to denote a jQuery object. If an object
 * acts like it's an element, it is most likely wrapped with jQuery.
 *
 * Also, game logic and validation is performed at server-side so you might wonder
 * why there is no game validation performed here. Validations performed here are
 * mostly for UI presentation, all moves will still be validated at server-side.
 *
 * If there are inconsistencies of data being recieved in the server side,
 * we may ultimately forfeit the current game session.
 *
 */
/**
 * @class GameBoard           This is the UI representation of the client side game board
 * @param {String}  selector  The selector of the containing element of the board
 * @param {Boolean} isPlayerA If this is the game creator or the game joiner (if that makes sense)
 */
var GameBoard = function(selector, isPlayerA) {
    this._container = $(selector);
    this._isPlayerA = isPlayerA;
    this._isAnimating = false;
    // locked means that the user is not allowed to move any pieces
    this.isLocked = false;
    this.isStarted = false;
    this.init();
    this.attachEventHandlers();
};
GameBoard.prototype = {
    /**
     * Assigns the positions from the virtual GameBoard to the UI
     * For more information regarding the virtual GameBoard, see
     * the server side version of this object
     */
    init: function() {
        // both players do not have the same positions in the board
        // so we separate them in creating a board view for them
        if (this._isPlayerA) {
            var row = 9, column = 0, position = 0, td;
            while (row > 0) {
                if (column == 0) {
                    row--;
                    column = 9;
                }
                td = this._container
                    .find('tr:nth-child(' + row + ') td:nth-child(' + column + ')')
                    // so we can easily find a TD element
                    .attr('data-pos', position)
                    // so we can easily refer the position value programmatically
                    .data('position', position);

                if (row > 5) {
                    td.addClass('initialized');
                }
                column--;
                position++;
            }
        } else {
            var row = 1, column = 1, position = 0;
            while (row < 9) {
                if (column > 9) {
                    row++;
                    column = 1;
                }
                td = this._container
                    .find('tr:nth-child(' + row + ') td:nth-child(' + column + ')')
                    // so we can easily find a TD element
                    .attr('data-pos', position)
                    // so we can easily refer the position value programmatically
                    .data('position', position);

                if (row > 5) {
                    td.addClass('initialized');
                }
                column++;
                position++;
            }
        }
    },
    // we housed this with a method so that we can do
    // some preparations the moment we start the game
    start: function(takeTurnCallback) {
        this.isStarted = true;
        this._container.find('td').removeClass('initialized');
        this.takeTurnCallback = takeTurnCallback;
    },
    /**
     * Create a new game piece element
     * @param {String} code     the rank code of the piece
     * @param {Number} position (optional) the position of the piece in the virtual board
     */
    createGamePieceElement: function(code, position) {
        // this is the containing jQuery object of this game piece
        var element = $('<div>');

        // add the classes for styling this piece object
        element.addClass('game-piece');

        // okay, we assume this is an opponent's game piece
        if (typeof code === 'number') {
            element.addClass('opponent');
            position = code;
            code = '';
        } else {
            element.addClass(code.toLowerCase());
            // this is just temporary until we can
            // create the images for each piece
            element.text(code);
        }

        // now let's add the game piece information
        element.data('piece', {
            code: code,
            position: position  || (position === 0 ? 0 : -1)
        });

        return element;
    },
    /**
     * Checks whether an element is a game piece or not
     * @param  {Object}  element A jquery object containing the questioned element
     * @return {Boolean}
     */
    isGamePieceElement: function(element) {
        if (element instanceof jQuery) {
            return element[0].className.indexOf('game-piece') !== -1;
        }
        return element.className.indexOf('game-piece') !== -1;
    },
    /**
     * Generate all pieces required for the game
     * and add them to the board UI
     */
    generatePieces: function() {
        var position = 0;
        if (!this._isPlayerA) {
            position = 45;
        }
        var elements = [];
        elements.push(this.createGamePieceElement('GA', position++));
        elements.push(this.createGamePieceElement('SPY', position++));
        elements.push(this.createGamePieceElement('SPY', position++));
        elements.push(this.createGamePieceElement('GEN', position++));
        elements.push(this.createGamePieceElement('LTG', position++));
        elements.push(this.createGamePieceElement('MG', position++));
        elements.push(this.createGamePieceElement('BG', position++));
        elements.push(this.createGamePieceElement('COL', position++));
        elements.push(this.createGamePieceElement('LTC', position++));
        elements.push(this.createGamePieceElement('MAJ', position++));
        elements.push(this.createGamePieceElement('CPT', position++));
        elements.push(this.createGamePieceElement('1LT', position++));
        elements.push(this.createGamePieceElement('2LT', position++));
        elements.push(this.createGamePieceElement('SGT', position++));
        elements.push(this.createGamePieceElement('FLG', position++));
        // we need 6 privates
        for (var i = 0; i < 6; i++) {
            elements.push(this.createGamePieceElement('PVT', position++));
        }
        this.addGamePieceElements(elements);
    },
    addGamePieceElements: function(elements) {
        // add them all to their corresponding positions in the board UI
        while (elements.length) {
            var element = elements.pop();
            this._container
                .find('td[data-pos="' + element.data('piece').position + '"]')
                .append(element);
        }
    },
    /**
     * NOTE: All these are just for client side styling, all moves are still
     *       validated in the game server.
     * Apply highlighting styles to TDs where the piece can possibly move
     * @param  {jQuery} piece The wrapping element of the game piece
     */
    highlightPiecePossibleMoves: function(piece) {
        var oldPos = piece.data('piece').position;
        var oldRow = Math.floor(oldPos / 9);
        // top
        highlightTD(this._container.find('td[data-pos="' + (oldPos + 9) + '"]'));
        // bottom
        highlightTD(this._container.find('td[data-pos="' + (oldPos - 9) + '"]'));
        // left and still on the same row
        if (oldRow == Math.floor((oldPos + 1) / 9)) {
            highlightTD(this._container.find('td[data-pos="' + (oldPos + 1) + '"]'));
        }
        // right and still on the same row
        if (oldRow == Math.floor((oldPos - 1) / 9)) {
            highlightTD(this._container.find('td[data-pos="' + (oldPos - 1) + '"]'));
        }

        function highlightTD(td) {
            if (td.length === 0) {
                // ooppsss, box is outside the game board
            }
            // okay, a valid board box but let's check if
            // we already have an existing game piece there
            // that is an opponent's game piece
            if (td.find('.game-piece.opponent').length) {
                // the let's add a "challengeable" style
                // which is only triggered when the player hovers
                // his/her mouse over this TD element
                td.addClass('challengeable');
            } else if (td.find('.game-piece').length == 0) {
                // no one's here so let's style this td
                td.addClass('possible-move');
            }
        }
    },
    /**
     * Move a piece to a new parent (and location) with animation
     * @param  {jQuery} element   The element to be moved
     * @param  {jQuery} newParent The target parent where
     *                            the @param:element will be moved to
     */
    movePiece: function(element, newParent) {

        // add a styling class of the target parent element
        newParent.removeClass('target');

        // let's cache our "this" object to self
        var self = this;
        // we do not accept any user movement
        this._isAnimating = true;

        // old offset location of the element
        var oldOffset = element.offset();
        // let's append it immediately so that
        // we can calculate the new location
        element.appendTo(newParent);
        var newOffset = newParent
                            .find('.game-piece')
                            .eq(0)
                            .offset();
        var oldPosition = element.parent().data('position');
        var newPosition = newParent.data('position');

        var cloned = element.clone().appendTo('body');
        cloned
            .css('position', 'absolute')
            .css('left', oldOffset.left)
            .css('top', oldOffset.top)
            .css('zIndex', 1000);

        // let's hide the original while we
        // animate the clone
        element.hide();
        cloned.animate({
                'top': newOffset.top,
                'left': newOffset.left
            }, 'fast', function() {
                // show the original element
                element.show();
                // now update the piece position from the new parent position
                element.data('piece').position = newParent.data('position');
                // the clone has served its purpose
                // so we'll remove it from the DOM
                cloned.remove();
                // remove the styling class
                newParent.removeClass('target');
                // done, we can now do another move
                self._isAnimating = false;
            });
    },
    /**
     * Clear any selected game piece
     */
    clearSelection: function() {
        // remove selection classes
        this._container
            .find('.game-piece')
            .removeClass('selected');
        // remove all classes for tds
        // but add initialized if
        // we haven't started yet
        this._container
            .find('td')
            .removeClass('challengeable')
            .removeClass('possible-move');
    },
    /**
     * Attach event handlers of anything you do with the board
     */
    attachEventHandlers: function() {
        var self = this;

        // clear selections when clicking outside a piece
        this._container.on('click', function(e) {
            if (!self._isAnimating && !self.isGamePieceElement(e.target)) {
                e.preventDefault();
                self.clearSelection();
            }
        });

        // selects a game piece
        this._container.delegate('.game-piece', 'click', function(e) {
            e.preventDefault();

            if (
                // we can't touch an opponent's piece
                this.className.indexOf('opponent') === -1
                // we can't move if it's not our turn yet
                && !self.isLocked
                // and we can't move if the UI is still animating
                && !self._isAnimating
                ) {
                // clear selections first
                self.clearSelection();
                // select this piece
                $(this).addClass('selected');
                // and if the game has already started and it's
                // obviously our turn, then highlight the possible moves
                if (self.isStarted) {
                    self.highlightPiecePossibleMoves($(this));
                }
            }
        });

        // attempts to move a game piece to a new location
        this._container.on('contextmenu', function(e) {
            e.preventDefault();

            // no user move is allowed at these states
            if (self.isLocked || self._isAnimating) {
                return;
            }

            // okay so it's our turn to move or we are still
            // on the arranging game pieces stage

            // if we don't have a selected game piece
            // then there is nothing to be moved
            var pieceToBeMoved = self._container.find('.game-piece.selected');
            if (!pieceToBeMoved.length) {
                return;
            }

            // if we are taking our turn, then we should display
            // all the possible moves that can be moved by the selected game piece
            if (self.isStarted) {

                var newParent = $(e.target);

                // oopppsss, it's not the parent,
                // the player may want to challenge an opponent
                // (or challenge his own piece, let's check that next)
                if (self.isGamePieceElement(e.target)) {
                    newParent = newParent.parent();
                }

                // okay, we are serious in moving this piece
                if (newParent.hasClass('challengeable') ||
                    newParent.hasClass('possible-move')) {
                    // we need to broadcast this move so that the socket
                    // can pick it up and let's see what the server will
                    // say about our move
                    self.takeTurnCallback(
                        pieceToBeMoved.data('piece'),
                        newParent.data('position')
                        );
                }

            // or if we are still in the arranging game pieces stage
            // then it's a free world as long as the moves are within
            // the player's bounderies
            } else {
                var newParent = $(e.target);
                // oopppsss, it's not the parent,
                // the player may want to swap game pieces
                if (self.isGamePieceElement(e.target)) {
                    newParent = newParent.parent();
                }

                // prevent moves outside the player's bounderies
                if ( self._isPlayerA && newParent.data('pos') > 26 ||
                    !self._isPlayerA && newParent.data('pos') < 45) {
                    self.clearSelection();
                    return;
                }

                // before we move, let's see first if
                // there is already a piece on the target parent
                var currentParent = pieceToBeMoved.parent();
                var existingPiece = newParent.find('.game-piece');

                // let's move it!!!
                self.movePiece(pieceToBeMoved, newParent);

                // if the target parent already has a piece,
                // we will swap both pieces
                if (existingPiece.length) {
                    self.movePiece(existingPiece, currentParent);
                }
            }
        });
    },
    getAllGamePieces: function() {
        var pieces = [];
        this._container.find('.game-piece').not('.opponent').each(function(index, element) {
            pieces.push($(element).data('piece'));
        });
        return pieces;
    }
}
