var util = require('util');

/**
 * @class GameDb
 * Used to track and manage games created
 */
function GameDb() {
    // A hashset, the games created using the game ID as key
    this._cache = {};
    // Used to track the number of games added since our hash
    // do not have a length property
    this.count = 0;
}

/**
 * Gather all game IDs
 * @return {Array} An array of String (game IDs)
 */
GameDb.prototype.getAllIds = function() {
    // all game IDs are also the key names of the games in the cache
    return Object.keys(this._cache);
};

/**
 * Get the game from the cache, null if game does not exist
 * @param  {String} gameId The game ID of the game to get
 * @return {Game}   Returns the game object
 */
GameDb.prototype.get = function(gameId) {
    return this._cache[gameId];
};

/**
 * Adds a new game to the cache
 * @param  {Game} game The game object
 * @return {Boolean}
 */
GameDb.prototype.add = function(game) {
    // we only add the game when it's not yet added to our cache
    if (!this._cache[game.id]) {

        // save this game and increase our counter
        this._cache[game.id] = game;
        this.count++;

        // let's augment game with some higher level data
        game.dateCreated = new Date();
        game.lastActivity = game.dateCreated;

        console.log('Game %s has been added to the game database. %d games online.', game.id, this.count);
        return true;
    }
    return false;
};

/**
 * Remove a game from the cache (to release memory)
 * @param  {String} gameId The game ID of the game to be removed
 * @return {Boolean}       Returns true on success, false otherwise
 */
GameDb.prototype.remove = function(gameId) {
    if (this._cache[gameId]) {
        delete this._cache[gameId];
        this.count--;
        console.log('Game has removed from database: ' + gameId);
        return true;
    }
    console.log('Game does not exist and so was not removed from database: ' + gameId);
    return false;
};

/**
 * @class       Game
 * @description The main application
 */
function Game() {
    // generate the game ID
    this.id = Game.generateUniqueId();

    // initialize our board and the first player
    this.board = new Board();
    this.playerA = new Player();
    this.playerA.id = Game.generateUniqueId();

    // set the game state to INITIALIZED
    this.state = Game.States.INITIALIZED;

    // lets declare our other expected properties to null
    this.currentPlayer = null;
    this.playerB = null;
}

/**
 * Create the second player
 */
Game.prototype.createPlayerB = function() {
    // we only create the next player when we are in INITIALIZED state
    if (this.state != Game.States.INITIALIZED) {
        throw new Error('Game is not in initialized state.');
    }
    // and that the second player is not yet set
    if (this.playerB) {
        throw new Error('Player 2 is already set.');
    }

    // now we create the next player
    this.playerB = new Player();
    this.playerB.id = Game.generateUniqueId();

    // the first player to take turn will always be player A
    this.currentPlayer = this.playerA;

    // change the state to READY
    this.state = Game.States.READY;
};

/**
 * A helper method to get the player object from a player ID.
 * Returns null if not found
 * @param  {String} playerId The ID of the player
 * @return {Player}          The player object
 */
Game.prototype.getPlayer = function(playerId) {
    return playerId == this.playerA.id ? this.playerA : playerId == this.playerB.id ? this.playerB : null;
};

/**
 * Set the player pieces in the board and perform validation
 * @param {String} playerId The player to associate the pieces to
 * @param {Array} pieces    The Array of GamePieces
 */
Game.prototype.setPlayerPieces = function(playerId, pieces) {
    // we need 21 pieces
    if (pieces.length != 21) {
        throw new Error('There should be 21 pieces.');
    }

    // get the player based from ID
    var player = this.getPlayer(playerId);

    // are we sure we have a valid player
    if (!player) {
        throw new Error('Player ID is invalid.');
    }

    // place the pieces to the board, throws exception upon validation failure
    this.board.placePieces(pieces, playerId == this.playerA.id);

    // associate the pieces to the player
    player.setPieces(pieces);

    // if both players have submitted their pieces,
    // then we'll change the game state to START
    if (this.playerA.hasPieces() && this.playerB.hasPieces()) {
        this.state = Game.States.START;
    }
};

/**
 * Decides who will win in a challenge
 * @param  {GamePiece} challenger The challenger game piece
 * @param  {GamePiece} challenged The challenged game piece
 * @return {Number}               Returns 0  for equal,
 *                                        1  if challenger wins,
 *                                        -1 if challenger loses
 */
Game.prototype.challenge = function(challenger, challenged) {
    // equal ranks that are not flag, both pieces are eliminated
    if (challenger.rank == challenged.rank && challenger.rank) {
        return 0;
    }
    // if challenger is a spy against anything not pvt or
    // if challenger is pvt and challenged is spy, challenger wins
    if (challenger.code == 'SPY' && challenged.code != 'PVT' ||
        challenger.code == 'PVT' && challenged.code == 'SPY') {
        return 1;
    }
    // if challenger is not a private and challenged is a spy or
    // challenger is a spy but challenged is pvt, challenger loses
    if (challenger.code != 'PVT' && challenged.code == 'SPY' ||
        challenger.code == 'SPY' && challenged.code == 'PVT') {
        return -1;
    }
    // if both flags, challenger wins
    if (challenger.rank == 0 && challenged.rank == 0) {
        return 1;
    }
    // otherwise, determine based on rank
    return challenger.rank > challenged.rank ? 1 : challenger.rank == challenged.rank ? 0 : -1;
};

/**
 * A player takes his/her turn
 * @param  {String} playerId    The player ID of the player who takes turn
 * @param  {GamePiece} piece    The game piece he/she will move
 * @param  {Number} newPosition The integer position of the piece in the game board
 * @return {Boolean}            Returns true on successful move, false otherwise
 */
Game.prototype.takeTurn = function(playerId, piece, newPosition) {
    // we should be in a start state to be able to move pieces
    if (this.state != Game.States.START) {
        throw new Error('Game state is invalid, must be START.');
    }
    // the current player should also be the player ID passed
    if (this.currentPlayer.id != playerId) {
        throw new Error('Player ID passed is not the current player');
    }
    var player = this.getPlayer(playerId);
    // let's move the piece, if a valid move, then change the current player and return true
    if (this.board.movePiece(player, piece, newPosition, this.challenge)) {
        this.currentPlayer = this.currentPlayer == this.playerA ? this.playerB : this.playerA;
        return true;
    }
    return false;
};

/**
 * Checks if the game is over. The current player is the game winner.
 * @return {Boolean}
 */
Game.prototype.isOver = function() {
    // it will be over if teh flag has reached the end
    // and the owner is also the current player
    if (this.board.hasFlagReachedEnd(this.currentPlayer, this.currentPlayer.id == this.playerA.id)) {
        this.state = Game.States.OVER;
        return true;
    }
    // it will be over when an opponent has no more flag piece in the board
    if (this.playerA.getFlag().position == -1) {
        this.state = Game.States.OVER;
        this.currentPlayer = this.playerB;
        return true;
    }
    // it will be over when an opponent has no more flag piece in the board
    if (this.playerB.getFlag().position == -1) {
        this.state = Game.States.OVER;
        this.currentPlayer = this.playerA;
        return true;
    }
    return false;
};

/**
 * A simple short UUID generator
 * @return {String} The ID generated
 */
Game.generateUniqueId = function() {
    return ('000000' + (Math.random() * Math.pow(36, 6) << 0).toString(36)).substr(-6);
};

/**
 * this is for testing only (unit testing), we generate pieces at client-side
 * although we validate them upon submition at server-side
 */
Game.generatePieces = function() {
    var pieces = [];
    pieces.push(new Piece('GA'));
    pieces.push(new Piece('SPY'));
    pieces.push(new Piece('SPY'));
    pieces.push(new Piece('GEN'));
    pieces.push(new Piece('LTG'));
    pieces.push(new Piece('MG'));
    pieces.push(new Piece('BG'));
    pieces.push(new Piece('COL'));
    pieces.push(new Piece('LTC'));
    pieces.push(new Piece('MAJ'));
    pieces.push(new Piece('CPT'));
    pieces.push(new Piece('1LT'));
    pieces.push(new Piece('2LT'));
    pieces.push(new Piece('SGT'));
    pieces.push(new Piece('FLG'));
    for (var i = 0; i < 6; i++) {
        pieces.push(new Piece('PVT'));
    }
    return pieces;
};

/**
 * @enum INITIALIZED the game is created
 *       READY       the game has 2 players joined
 *       START       the players are taking turns
 *       OVER        the game is over
 */
Game.States = {
    INITIALIZED: 0,
    READY: 1,
    START: 2,
    OVER: 3
};

/**
 * @class          Player
 * @description    Represents the client side player data and behaviour
 */
function Player() {
    // let's initialize our properties
    this.id = 0;
    this.name = 'Anonymous';
    this.pieces = null;
}

/**
 * Associate the pieces to this player
 * @param {Array} pieces The game pieces of this player
 */
Player.prototype.setPieces = function(pieces) {
    // the game checks the number of pieces
    // the board checks the positioning of the pieces
    // now we check if all ranks are complete
    var allRanks = [
        'GA', 'GEN', 'LTG', 'MG', 'BG', 'COL', 'LTC', 'MAJ', 'CPT', '1LT',
        '2LT', 'SGT', 'PVT', 'PVT', 'PVT', 'PVT', 'PVT', 'PVT', 'SPY', 'SPY', 'FLG'
        ];

    for (var i = 0, j = pieces.length; i < j; i++) {
        var index = allRanks.indexOf(pieces[i].code);

        if (index !== -1) {
            allRanks.splice(index, 1);
        } else {
            break;
        }
    }
    if (allRanks.length) {
        throw new Error('The following ranks are NOT found: ' + JSON.stringify(allRanks));
    }
    // so we are good
    this.pieces = pieces;
};

/**
 * Checks if the player already has pieces (and complete)
 * @return {Boolean}
 */
Player.prototype.hasPieces = function() {
    return this.pieces != null && this.pieces.length == 21;
};

/**
 * Checks whether this player has the parameter piece
 * @param  {Piece}   piece The game piece to check
 * @return {Boolean}
 */
Player.prototype.hasPiece = function(piece) {
    return this.pieces != null && this.pieces.indexOf(piece) !== -1;
};

/**
 * Get the flag piece of this player
 * @return {Piece} The Flag game piece
 */
Player.prototype.getFlag = function() {
    // we will cache the flag, btw
    if (!this.flag) {
        for (var i = 0; i < this.pieces.length; i++) {
            if (this.pieces[i].isFlag()) {
                this.flag = this.pieces[i];
                break;
            }
        }
    }
    return this.flag;
};

/**
 * @class       Piece
 * @description The rank is used in comparing, while value is used for computing when the game is a draw
 *              A position of -1 means the game piece is not on the game board
 * @param       {string} code the 2/3 digit code of the piece
 */
function Piece(code) {
    switch(code) {
        case 'GA' : this.rank = 14; this.value = 7.80; break;
        case 'SPY': this.rank = 13; this.value = 7.50; break;
        case 'GEN': this.rank = 12; this.value = 6.95; break;
        case 'LTG': this.rank = 11; this.value = 6.15; break;
        case 'MG' : this.rank = 10; this.value = 5.40; break;
        case 'BG' : this.rank =  9; this.value = 4.70; break;
        case 'COL': this.rank =  8; this.value = 4.05; break;
        case 'LTC': this.rank =  7; this.value = 3.45; break;
        case 'MAJ': this.rank =  6; this.value = 2.90; break;
        case 'CPT': this.rank =  5; this.value = 2.40; break;
        case '1LT': this.rank =  4; this.value = 1.95; break;
        case '2LT': this.rank =  3; this.value = 1.55; break;
        case 'SGT': this.rank =  2; this.value = 1.20; break;
        case 'PVT': this.rank =  1; this.value = 1.37; break;
        case 'FLG': this.rank =  0; this.value = 0.00; break;
        default: throw new Error('Game piece code is invalid: ' + code);
    }
    this.code = code;
    this.position = -1;
}

Piece.prototype.isFlag = function() {
    return this.code == 'FLG';
};

/**
 * @class       Board
 * @description A single-dimensional array representing the 9x8 board
 */
function Board() {
    this.pieces = new Array(72);
}

/**
 * Place the pieces to the board but perform validation before that
 * @param  {Array}  pieces    the game pieces
 * @param  {Boolean} isPlayerA Flag if the pieces are for the main player or not
 */
Board.prototype.placePieces = function(pieces, isPlayerA) {

    // the players start and end positions differ in the board
    // so we distinguish them here
    var minPos = isPlayerA ?  0 : 45,
        maxPos = isPlayerA ? 26 : 71;

    // first, validate the pieces' positions
    var takenPositions = [];
    for (var i = 0; i < pieces.length; i++) {
        var piece = pieces[i];
        // check if a piece has a position
        if (piece.position == -1) {
            throw new Error(util.format('[%s] has invalid position: -1', piece.code));
        }

        // check if the position is within range
        if (piece.position < minPos || piece.position > maxPos) {
            throw new Error(util.format('[%s]\'s position (%d) is out of range (%d, %d).', piece.code, piece.position, minPos, maxPos));
        }
        
        // check if there are no duplicate positions
        if (takenPositions.indexOf(piece.position) == -1) {
            takenPositions.push(piece.position);
        } else {
            throw new Error(util.format('[%s]\'s position (%d) is duplicate.', piece.code, piece.position));
        }

        // check if there is no other piece for that position
        if (this.pieces[piece.position] != null) {
            throw new Error(util.format('[%s]\'s position (%d) is taken.', piece.code, piece.position));
        }
    }

    // it passed so we set the board pieces
    for (var i = 0; i < pieces.length; i++) {
        this.pieces[pieces[i].position] = pieces[i];
    }
};

/**
 * Let's try to move the piece in the game board
 * @param  {Player}   player            The player who moves his/her piece
 * @param  {Piece}    piece             The game piece to be moved
 * @param  {Number}   newPosition       The new position of the game piece in the board
 * @param  {Function} challengeCallback If an opponent piece is present in the target position,
 *                                      then we have a challenge and we need to determine who will remain
 * @return {Boolean}                    Returns true on successful move, false otherwise
 */
Board.prototype.movePiece = function(player, piece, newPosition, challengeCallback) {

    // check if the piece is actuall moving
    if (piece.position == newPosition || !this.isNewPositionValid(piece.position, newPosition)) {
        return false;
    }

    // check if we are moving on our own piece's position
    var newPositionPiece = this.pieces[newPosition];
    if (player.hasPiece(newPositionPiece)) {
        // you can't challenge your own piece
        return false;
    }

    // ok, we can safely move the piece
    // remove the piece from the board
    this.pieces[piece.position] = null;
    piece.position = -1;

    // if this is not a challenge OR you won the challenge, then move your piece
    if (newPositionPiece == null || challengeCallback(piece, newPositionPiece) != -1) {
        if (newPositionPiece) {
            // since this is a challenge and you beat the newPositionPiece
            // remove this from the board
            newPositionPiece.position = -1;
        }
        // let's move to the target position
        this.pieces[newPosition] = piece;
        piece.position = newPosition;
    }
    return true;
};

/**
 * Checks if the new position is a valid position in the game Board
 * A valid position is forward, backward and sideways only (1 block)
 * @param  {Number}  oldPos The old position
 * @param  {Number}  newPos The new position
 * @return {Boolean}
 */
Board.prototype.isNewPositionValid = function(oldPos, newPos) {
    return (
        oldPos - newPos == 9 || newPos - oldPos == 9 ||
        oldPos + 1 == newPos || oldPos - 1 == newPos
    );
};

/**
 * Checks whether the player's flag reached the opponent's end of the board
 * @param  {Player}  player    The player who moves his flag
 * @param  {Boolean} isPlayerA Flag if this is the main player (for determining the correct board position)
 * @return {Boolean}
 */
Board.prototype.hasFlagReachedEnd = function(player, isPlayerA) {
    return (
        player.getFlag().position >= (isPlayerA ? 63 : 0)
        && player.getFlag().position <= (isPlayerA ? 71 : 8)
    );
};

/**
 * Let's bring all these objects outside
 */
module.exports = {
    Game: Game,
    Piece: Piece,
    Player: Player,
    Board: Board,
    GameDb: GameDb
};
