// Import our game module
var gameModule = require('./game.js');

// let's instantiate our game in-memory database
// Our game does not really track a game after it's over
// This is like "play the game and forget after" so we don't
// really need persistence although to be able to scale,
// we may need to do that but for now, this will serve us.
// Let's tackle scalability later on
var gameDb = new gameModule.GameDb();

// The socket io object and the client socket object
var io, socket;

/**
 * This function is called by index.js to handle io communications
 * @param io     The Socket.IO object
 * @param socket The Socket object from the connecting client
 */
exports.handle = function(_io, _socket) {

    io = _io;
    socket = _socket;

    socket.on('game-create', onGameCreate);
    socket.on('player-join', onPlayerJoin);
    socket.on('submit-pieces', onSubmitPieces);
    socket.on('player-turn-taken', onPlayerTurnTaken);
    socket.on('disconnect', onPlayerDisconnect);

};

// NOTES:
// - All emitted events must have a "success" boolean flag to determine success or failure
// - All error emitted events must have a "message" property that contains the error message

/**
 * Handles the "create" IO event
 * Emits the "created" IO event to be handled at client side
 * Called when a player at client side creates a new game
 * @param   data contains the data given from the source of the event
 *          - playerName
 * @private this refers to the current client socket
 */
function onGameCreate(data) {

    // let's do a clean up everytime a new game is requested
    // NOTE: Although we can do this outside of the application
    // but we can do this for now, let's just take note.
    purgeGameDb();

    // we need a player name to start
    if (data.playerName) {

        // let's create a new game
        var game = new gameModule.Game();
        // although we don't really use it,
        // let's add the name of the main player
        game.playerA.name = data.playerName;

        // add it to our game database
        gameDb.add(game);

        // let's go to a game room
        this.join(game.id);
        // then we will let the client know that we are done
        this.emit('game-created', {
            success: true,
            gameId: game.id,
            playerId: game.playerA.id,
            playerName: data.playerName
        });

        console.log('A new game room has been created: %s', game.id);

    } else {
        // we need a player name and this must have been validated at client-side
        this.emit('game-created', {
            success: false,
            message: 'Player Name was not supplied. No game was created.'
        });

        console.log('No game was created, missing player name.');
    }
}

/**
 * Handles the "join" IO event
 * Emits the "joined" IO event for client side
 * Called when a new client connects to an existing game
 * @param   data contains the data given from the source of the event
 *          - gameId, playerName
 * @private this refers to the current client socket
 */
function onPlayerJoin(data) {

    // let's get the game from the database
    var game = gameDb.get(data.gameId);

    // the game ID must have been invalid
    if (!game) {
        this.emit('player-joined', {
            success: false,
            message: 'The game ID given does not refer to an existing game session.'
        });
        return;
    }

    // we also require the player name for player B
    if (!data.playerName) {
        this.emit('player-joined', {
            success: false,
            message: 'Player Name was not supplied. Unable to join game.'
        });
        return;
    }

    try {

        // okay, let's TRY to join to the game (if possible)
        // these lines of codes will throw exception
        // when the game state is not valid
        game.createPlayerB();
        game.playerB.name = data.playerName;

        // it looks like we are fine so, join the game room
        this.join(game.id);

        // and emit the "joined" event in the same room
        io.sockets.in(game.id).emit('player-joined', {
            success: true,
            gameId: game.id,
            playerId: game.playerB.id,
            playerName: data.playerName,
            opponentName: game.playerA.name
        });

        console.log('Player %s joined game %s.', game.playerB.id, game.id);
    } catch (error) {
        console.log(error);
        this.emit('player-joined', {
            success: false,
            message: error.message
        });
    }
}

/**
 * Handles the "submit pieces" IO event
 * Emits the "pieces submitted" IO event
 * @param  data The data required for this event:
 *         - gameId, playerId, gamePieces
 */
function onSubmitPieces(data) {

    // let's get the game from the database
    var game = gameDb.get(data.gameId);

    // the game ID must have been invalid
    if (!game) {
        this.emit('player-joined', {
            success: false,
            message: 'The game ID given does not refer to an existing game session.'
        });
        return;
    }

    // do we have a player ID
    if (!data.playerId) {
        this.emit('pieces-submitted', {
            success: false,
            message: 'Missing player ID.'
        });
        return;
    }

    // do we have game pieces
    if (!data.gamePieces) {
        this.emit('pieces-submitted', {
            success: false,
            message: 'Missing game pieces.'
        });
        return;
    }

    try {
        // okay, let's do this, this method throws excetions for validation errors
        game.setPlayerPieces(data.playerId, data.gamePieces);

        var positions = [];
        for (var i = 0; i < data.gamePieces.length; i++) {
            positions.push(data.gamePieces[i].position);
        }

        // we emit the "pieces submitted" IO event for notification purposes
        io.sockets.in(game.id).emit('pieces-submitted', {
            success: true,
            playerId: data.playerId,
            positions: positions
        });

        // so the pieces are set in the game board, we would like
        // to broadcast an event if both players have submitted the
        // game pieces since that will be the cue to START the game
        if (game.state == gameModule.Game.States.START) {
            io.sockets.in(game.id).emit('player-take-turn', {
                // we also passed a "justStarted" flag to notify
                // the clients to prepare anything they want since
                // the game will be started
                // NOTE: I first thought of emitting two events,
                //       "start-game" and "player-take-turn" but we can't
                //       be sure which of the events will reach the clients
                //       first so we do this a little ugly workaround. There
                //       must be a better way.
                justStarted: true,
                playerId: game.currentPlayer.id
            });
        }

        console.log('Player %s has submitted his pieces.', data.playerId);

    } catch (error) {
        console.log(error);
        this.emit('pieces-submitted', {
            success: false,
            message: error.message
        });
    }
}

/**
 * Handles the IO event where the player submits his/her move to the server
 * Emits the "player-take-turn" event if the game is not yet OVER
 * @param  data The data required for this event:
 *         - gameId, playerId, piece, newPosition
 */
function onPlayerTurnTaken(data) {

    // let's get the game from the database
    var game = gameDb.get(data.gameId);

    // the game ID must have been invalid
    if (!game) {
        this.emit('player-take-turn', {
            success: false,
            message: 'The game ID given does not refer to an existing game session.'
        });
        return;
    }

    // do we have a player ID
    if (!data.playerId) {
        this.emit('pieces-submitted', {
            success: false,
            message: 'Missing player ID.'
        });
        return;
    }
}

function onPlayerDisconnect() {
    // let's emit an event ONLY when we are not in the default blank room
    for (var room in this.manager.rooms) {
        if (room) {
            // this client has joined this non-empty room, hence we will notify
            // all the clients of this room that this client has left
            io.sockets.in(room.substr(1)).emit('player-left');
            console.log('Socket client %s has disconnected.', this.id);
        }
    }
}

/**************************************************************
 UTILITY FUNCTIONS
 **************************************************************/

/**
 * Cleans up our database for orphaned games
 */
function purgeGameDb() {
    // io.sockets.manager.rooms is a hash, with the room name as a key to an array of socket IDs.
    var gameIds = [];
    for (var key in io.sockets.manager.rooms) {
        // NOTE: The room names will have a leading / character.
        gameIds.push(key.substr(1));
    }

    // now let's get all game ids from our game database
    var gameDbIds = gameDb.getAllIds();
    // let's keep track of the games deleted
    var deleted = 0;

    // now, delete the game if it's not being tracked by socket.io
    for (var i in gameDbIds) {
        var gameId = gameDbIds[i];
        if (gameIds.indexOf(gameId) == -1 && gameDb.remove(gameId)) {
            deleted++;
        }
    }
    console.log('%d games have been deleted from memory.', deleted);
}
