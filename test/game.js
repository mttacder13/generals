var assert = require('assert');

describe('Game', function() {

    var gameModule = require('../app/game.js'),
        gameDb = new gameModule.GameDb(),
        game = new gameModule.Game();

    describe('in-memory games db tests', function() {
        it('should return null for non-existing games', function() {
            assert(gameDb.get('xxxxxx') == null);
        });
        var game = new gameModule.Game();
        it('should return correct game for existing games', function() {
            gameDb.add(game);
            assert(gameDb.get(game.id) == game);
            assert(gameDb.count == 1);
        });
        it('should throw exception for setting existing games', function() {
            assert(gameDb.add(game) == false);
        });
        it('should delete the correct game', function() {
            gameDb.remove(game.id);
            assert(gameDb.count == 0);
            assert(gameDb.get(game.id) == null);
        });
        it('should return false for deleting non-existing game', function() {
            assert(gameDb.remove(game.id) == false);
        });
    });

    describe('initialization', function() {
        it('game id should have value', function() {
            assert(game.id != null && game.id.length > 0);
            console.log(game.id);
        });
        it('state should be initialized', function() {
            assert(game.state == gameModule.Game.States.INITIALIZED);
        });
        it('player one should have ID', function() {
            assert(game.playerA.id != null && game.playerA.id.length > 0);
            console.log(game.playerA.id);
        });
        it('player two should be null', function() {
            assert(game.playerB == null);
        });
    });

    describe('player two joins', function() {
        it('player two should have ID', function() {
            game.createPlayerB();
            assert(game.playerB != null && game.playerB.id.length > 0);
            console.log(game.playerB.id);
        });
        it('player two should not be set twice or more', function() {
            assert.throws(function() {
                game.createPlayerB();
            }, Error);
        });
        it('state should now be ready', function() {
            assert(game.state == gameModule.Game.States.READY);
        });
        it('getPlayer() should return correct player passing their ids, null for invalid', function() {
            assert(game.getPlayer(game.playerA.id) == game.playerA);
            assert(game.getPlayer(game.playerB.id) == game.playerB);
            assert(game.getPlayer('xxx') == null);
        });
    });

    describe('setting the pieces', function() {
        var pieces = null;
        it('should not accept less than 21 pieces', function() {
            pieces = gameModule.Game.generatePieces();
            pieces.pop();
            assert.throws(function() {
                game.setPlayerPieces(game.playerA.id, pieces);
            }, Error);
        });
        it('should not accept pieces with no positions', function() {
            pieces = gameModule.Game.generatePieces();
            assert.throws(function() {
                game.setPlayerPieces(game.playerA.id, pieces);
            }, Error);
        });
        it('should accept 21 pieces', function() {
            pieces = gameModule.Game.generatePieces();
            for (var i = 0; i < 21; i++) {
                pieces[i].position = i;
            }
            assert.doesNotThrow(function() {
                game.setPlayerPieces(game.playerA.id, pieces);
            });
        });
        it('should not accept pieces if it has already been set', function() {
            assert.throws(function() {
                game.setPlayerPieces(game.playerA.id, pieces);
            }, Error);
        });
        it('state should be start after both players have pieces', function() {
            pieces = gameModule.Game.generatePieces();
            for (var i = 45, j = 0; j < 21; i++, j++) {
                pieces[j].position = i;
            }
            game.setPlayerPieces(game.playerB.id, pieces);
            assert(game.state == gameModule.Game.States.START);
        });
        it('current player should not be player two', function() {
            assert(game.currentPlayer != game.playerB);
        });
        it('current player should be player one', function() {
            assert(game.currentPlayer == game.playerA);
        });
    });

    describe('pieces', function() {
        it('should return GA for GA piece', function() {
            assert(new gameModule.Piece('GA').code == 'GA');
        });
        it('should return 0 for flag piece value', function() {
            assert(new gameModule.Piece('FLG').value == 0);
        });
        it('should return true if 2star general > captain', function() {
            assert(new gameModule.Piece('MG').value > new gameModule.Piece('CPT').value);
        });
    });

    describe('challenge', function() {

        it('should return 0 for GA vs GA', function() {
            var p1 = new gameModule.Piece('GA'),
                p2 = new gameModule.Piece('GA');
            assert(game.challenge(p1, p2) == 0);
        });

        it('should return 1 for GA vs CPT', function() {
            var p1 = new gameModule.Piece('GA'),
                p2 = new gameModule.Piece('CPT');
            assert(game.challenge(p1, p2) == 1);
        });

        it('should return -1 for GEN vs SPY', function() {
            var p1 = new gameModule.Piece('GEN'),
                p2 = new gameModule.Piece('SPY');
            assert(game.challenge(p1, p2) == -1);
        });

        it('should return 1 for FLG vs FLG, first flag wins', function() {
            var p1 = new gameModule.Piece('FLG'),
                p2 = new gameModule.Piece('FLG');
            assert(game.challenge(p1, p2) == 1);
        });

        it('should return -1 for SPY vs PVT', function() {
            var p1 = new gameModule.Piece('SPY'),
                p2 = new gameModule.Piece('PVT');
            assert(game.challenge(p1, p2) == -1);
        });

        it('should return 1 for PVT vs SPY', function() {
            var p1 = new gameModule.Piece('PVT'),
                p2 = new gameModule.Piece('SPY');
            assert(game.challenge(p1, p2) == 1);
        });

        it('should return 0 for SPY vs SPY', function() {
            var p1 = new gameModule.Piece('SPY'),
                p2 = new gameModule.Piece('SPY');
            assert(game.challenge(p1, p2) == 0);
        });

    });

    describe('taking turns', function() {

        it('should throw exception when invalid player id is passed', function() {
            // first player's turn
            assert.throws(function() {
                game.takeTurn(game.playerB.id);
            });
        });

        it('should not change the position of GA when moved', function() {
            // player one GA cannot be moved
            assert(game.takeTurn(game.playerA.id, game.board.pieces[0], 1) == false);
        });

        it('should change the position of last PVT when moved', function() {
            var pvt = game.board.pieces[20];
            // player one PVT can be moved to position 29
            assert(game.takeTurn(game.playerA.id, pvt, 29) == true);
            // player one PVT  changed it's position
            assert(pvt.position == 29);
            // no one is in position 20 now
            assert(game.board.pieces[20] == null);
        });

        it('should change the current player', function() {
            // current player should be two
            assert(game.currentPlayer == game.playerB);
        });

        it('should change the position of movable pieces', function() {
            // move player two SPY to position 38
            assert(game.takeTurn(game.playerB.id, game.board.pieces[47], 38) == true);
            // move player one PVT to position 20
            assert(game.takeTurn(game.playerA.id, game.board.pieces[19], 20) == true);
            // move player two FLG to position 68
            assert(game.takeTurn(game.playerB.id, game.board.pieces[59], 68) == true);
            // cannot move player one PVT to position 29 since that has also his own PVT
            assert(game.takeTurn(game.playerA.id, game.board.pieces[20], 29) == false);
            // move player one PVT to position 27
            assert(game.takeTurn(game.playerA.id, game.board.pieces[18], 27) == true);
            // move player two GA to position 36
            assert(game.takeTurn(game.playerB.id, game.board.pieces[45], 36) == true);
            // unmovable pieces being tried to be moved
            // or jumped on more squares which should not be possible
            assert(game.takeTurn(game.playerA.id, game.board.pieces[6], 42) == false);
            assert(game.takeTurn(game.playerA.id, game.board.pieces[17], 44) == false);
            assert(game.takeTurn(game.playerA.id, game.board.pieces[1], 71) == false);
        });

        it('should remove the spy when pvt challenges the spy', function() {
            var spy = game.board.pieces[38];
            assert(game.takeTurn(game.playerA.id, game.board.pieces[29], 38) == true);
            assert(spy.position == -1);
        });

        it('should remove the pvt when ga challenges the pvt', function() {
            var pvt = game.board.pieces[27];
            assert(game.takeTurn(game.playerB.id, game.board.pieces[36], 27) == true);
            assert(pvt.position == -1);
        });

        it('should not be over at this point', function() {
            assert(game.isOver() == false);
        });

        it('should be game ALMOST over by these moves', function() {
            assert(game.takeTurn(game.currentPlayer.id, game.board.pieces[14], 23) == true);
            assert(game.takeTurn(game.currentPlayer.id, game.board.pieces[50], 41) == true);
            assert(game.takeTurn(game.currentPlayer.id, game.board.pieces[23], 32) == true);
            assert(game.isOver() == false);
        });
        // basic winner: player2
        it('should be game over by this move', function() {
            assert(game.takeTurn(game.currentPlayer.id, game.board.pieces[41], 32) == true);
            assert(game.isOver() == true);
            assert(game.state == gameModule.Game.States.OVER);
            assert(game.currentPlayer == game.playerB);
            assert.throws(function() {
                game.takeTurn(game.currentPlayer.id, game.board.pieces[41], 32);
            });
        });

    });

});
