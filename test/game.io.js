var io = require('socket.io-client'),
    assert = require('assert');

describe('Testing a complete game session', function() {

    var socket, server, currentGameId;

    before(function(done) {

        server = require('../app/index.js').server;
        console.log('HTTP server running...');

        socket = io.connect('http://localhost:' + server.address().port, {
            'reconnection delay': 0,
            'reopen delay': 0,
            'force reconnection': true
        });
        socket.on('connect', function() {
            console.log('Client socket connected...');
            done();
        });
        socket.on('disconnect', function() {
            console.log('Client socket disconnected...');
        });
    });

    after(function(done) {
        if (socket.socket.connected) {
            console.log('Client socket disconnecting...');
            socket.disconnect();
        } else {
            console.log('No connection to break.');
        }
        server.close();
        console.log('HTTP server is closed.');
        done();
    })

    describe('Creating a game room', function() {

        it('should emit the created event with success false when no player name provided', function(done) {
            socket.emit('game-create', {});
            socket.once('game-created', function(data) {
                assert(data.success === false);
            });
            done();
        });

        it('should emit the created event with success true when a player name is provided', function(done) {
            socket.emit('game-create', { playerName: 'Jojo' });
            socket.once('game-created', function(data) {
                assert(data.success);
                currentGameId = data.gameId;
            });
            done();
        });

    });

    describe('Joining a game room', function() {

        it('should emit the joined event with success false when passing invalid game ID', function(done) {
            socket.emit('player-join', { gameId: 'xxx' });
            socket.once('player-joined', function(data) {
                assert(data.success === false);
            });
            done();
        });

        it('should emit the joined event with success true when passing correct parameters', function(done) {
            socket.emit('player-join', { gameId: currentGameId, playerName: 'Joy' });
            socket.once('player-joined', function(data) {
                assert(data.success);
            });
            done();
        });

        it('should emit the joined event with success false when a player has already joined (preceeding test)', function(done) {
            socket.emit('player-join', { gameId: currentGameId, playerName: 'Joy' });
            socket.once('player-joined', function(data) {
                assert(data.success == false);
            });
            done();
        });

    });

});
